---
layout: markdown_page
title: "Contracts"
---
* [Disclaimer](#disclaimer)
* [PIAA](#PIAA)
* [GitLab Inc employee offer letter](#inc-employee)
* [GitLab BV employee offer letter](#bv-employee)
* [GitLab Inc contractor agreement](#inc-contractor)
* [GitLab BV contractor agreement](#bv-contractor)

## Disclaimer <a name="disclaimer"></a>
Enter legal disclaimer here. I.e. these are typical legal forms, that are subject to change. (Check with legal counsel on good wording)

## PIAA<a name="PIAA"></a>
Enter the PIAA here.

## GitLab Inc employee offer letter<a name="inc-employee"></a>
Enter the GitLab Inc employee offer letter here, indicating optional clauses on stock options, double triggers, and commission.

## GitLab BV employee offer letter<a name="bv-employee"></a>
Enter the GitLab BV employee offer letter here (in Dutch?). How does this deal with stock options?

## GitLab Inc contractor agreement<a name="inc-contractor"></a>
Enter the GitLab Inc contractor agreement here.

## GitLab BV contractor agreement<a name="bv-contractor"></a>
Enter the GitLab BV contractor agreement here (in Dutch?).